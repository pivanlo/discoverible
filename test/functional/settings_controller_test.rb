require 'test_helper'

class SettingsControllerTest < ActionController::TestCase
  test "should get account" do
    get :account
    assert_response :success
  end

  test "should get import" do
    get :import
    assert_response :success
  end

end
