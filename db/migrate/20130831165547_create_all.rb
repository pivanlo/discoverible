class CreateAll < ActiveRecord::Migration
  def change    
    create_table :collections do |t|
      t.string :channel_id
      t.string :title
      t.string :thumbnail
      t.timestamps
    end
    
    create_table :resources do |t|
      t.string :url
      t.string :title
      t.boolean :dirty, default: false
      t.belongs_to :collection
      t.timestamps
    end
    
    create_table :facets do |t|
      t.string :name
      t.integer :position
      t.belongs_to :collection
      t.timestamps
    end
    
    create_table :values do |t|
      t.belongs_to :facet
      t.string :name
      t.integer :resources_count, default: 0
      t.timestamps
    end
    
    create_table :resources_values, id: false do |t|
      t.belongs_to :resource
      t.belongs_to :value
    end
    
    # add_index :resources, :collection_id
  end
end
