Discoverible::Application.routes.draw do
  # API
  get 'facets', to: 'facets#index'
  post 'collections/update'
  get 'results', to: 'results#index'
  get 'collections/typeahead', to: 'collections#typeahead'
  
  # Pages
  root to: 'home#index'
  get 'signin', to: 'home#create_collection'
  get '/youtube/:channel_id', to: 'collections#show'
  get '/youtube/:channel_id/edit', to: 'collections#edit'
  get 'contact', to: 'contact#index'
  
  # API
  get 'settings/authorize_upload'
  get 'settings/process_image'
  post 'settings/update'
  get 'sessions/create'
  get 'sessions/destroy'
  get 'users/create'
  
  post 'add_facet', to: 'facets#create'
  post 'facets/update'
  post 'delete_facet', to: 'facets#destroy'
  post 'reorder', to: 'facets#reorder'
  post 'rename_value', to: 'values#update'
  post 'delete_value', to: 'values#destroy'
  post 'resources/edit', to: 'resources#edit'
  post 'resources/edit_all', to: 'resources#edit_all'
  post 'resources', to: 'resources#create'
  delete 'resources/:id', to: 'resources#destroy'
end
