// http://stackoverflow.com/a/18948076
/*
ko.observable.fn.refresh = function () {
  var data = this();
  this(null);
  this(data);
};
*/
ko.observableArray.fn.refresh = function () {
  var data = this();
  this([]);
  this(data);
};
