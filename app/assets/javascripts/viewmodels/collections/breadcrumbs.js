di.collections.breadcrumbs = function () {
  var values = ko.observableArray();
  
  var init = function () {
    $.subscribe('selectedValuesUpdate', update)
  };

  var update = function(selectedValues) {
    //values([]);
    values(selectedValues);
  };

  return {
    init: init,
    values: values,
    update: update
  };
};
