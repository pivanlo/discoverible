di.collections.dataService = function () {
  var RESULTS_PER_PAGE = 10;
  var viewModel, outOfSync;
  var currentRequestId = 0; // used to abort requests (http://stackoverflow.com/a/9999813/2669222)

  var init = function (_viewModel) {
    viewModel = _viewModel;
    outOfSync = di.collectionData.oos ? true : false;
    $.subscribe('pathChange', prepareSearch);
    $.subscribe('outOfSyncChange', setOutOfSync);
  };
  
  var setOutOfSync = function (value) {
    outOfSync = value;
  };
  
  var prepareSearch = function (page, values) {
    var offset = (page - 1) * RESULTS_PER_PAGE;
    if (offset < 0) {
      offset = 0;
    }
    var callback = function (selectedValues) {
      search(offset, selectedValues);
    };
    $.publish('selectedValuesChange', [values, callback]);
  };
  
  var getFacets = function (callback) {
    $.get('/facets?',{id: di.collectionData.id}, function (data) {
      callback(data);
    });
  };
  
  var search = function (offset, selectedValues) {
    if (outOfSync) {
      searchSecondary(offset, selectedValues);
    } else {
      searchPrimary(offset, selectedValues);
    }
  };
  
  var searchPrimary = function (offset, selectedValues) {
    var requestId = ++currentRequestId;
    var valueIds = _.pluck(selectedValues, 'id');
    data = {
      "query": { "match_all": {} },
      "filter": { "term": { "collection_id": di.collectionData.id } },
      "from": offset,
      "sort" : {id: "asc"},
      "facets" : { "values" : { "terms" : {"field" : "values", size: 9999} } }
    };
    if (valueIds.length) {
      data.query = { "terms": { "values": valueIds, "minimum_should_match": valueIds.length } };
    }
    $('.player').addClass('player-hidden');
    $('#waiting').show();
    $.ajax({
      url: 'http://api.discoverible.com/_search?source=' + JSON.stringify(data),
      dataType: 'jsonp',
      contentType: 'application/json; charset=UTF-8',
      success: function(data) {
        if (requestId !== currentRequestId) return;
        if (typeof data === 'string') data = $.parseJSON(data);
        data.resources = _.pluck(data.hits.hits, '_source');
        data.count = data.hits.total;
        data.terms = data.facets.values.terms;
        onSearchSuccess(data, selectedValues, offset);
      }
    });
  };
  
  var searchSecondary = function (offset, selectedValues) {
    var requestId = ++currentRequestId;
    var data = {
      collectionId: di.collectionData.id,
      values: _.pluck(selectedValues, 'id'),
      offset: offset
    };
    $('.player').addClass('player-hidden');
    $('#waiting').show();
    var ajax = $.ajax({
      url: '/results',
      data: data,
      success: function (data) {
        if (requestId !== currentRequestId) return;
        if (typeof data === 'string') data = $.parseJSON(data);
        outOfSync = data.oos;
        onSearchSuccess(data, selectedValues, offset);
      }
    });
  };
  
  var onSearchSuccess = function (data, selectedValues, offset) {
    repeatSearchIfTooMuchOffset(data, selectedValues, offset);
    $('#waiting').hide();
    window.scrollTo(0, 0);
    var page = (offset / RESULTS_PER_PAGE) + 1;
    $.publish('resultsReady', [data, page]);
    $('body').hide().show(); // see comment at the bottom [1]
  };
  
  var repeatSearchIfTooMuchOffset = function (data, selectedValues, offset) {
    if (data.resources.length === 0 && data.count > 0) {
      offset = Math.max(0, data.count - 10);
      offset = Math.ceil(offset / 10) * 10;
      search(offset, selectedValues);
      return;
    }
  };
  
  return {
    init: init,
    getFacets: getFacets,
    search: search,
    setOutOfSync: setOutOfSync
  };
};

// [1] Force reflow. 
// IE doesn't update the window height after rendering new results.
// Instead, it shows empty space at the end.


















