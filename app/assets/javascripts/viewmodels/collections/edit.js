di.collections.edit = function () {
  var init = function () {
    this.dataService.init(this);
    this.collection.init(this);
    this.facetMenu.init(this);
    this.resourceEditor.init(this);
    this.batchEditor.init(this);
    this.user.init(this);
    this.modal.init(this);
    this.profileEditor.init(this);
    this.results.init(this);
    this.facetEditor.init(this);
    this.facetsReorder.init(this);
    this.pagination.init(this);
    this.breadcrumbs.init(this);
    this.channelSearch.init(this);
    window.vm = this; // for debugging
    return this;
  };
  
  return {
    page: 'edit',
    init: init,
    dataService : di.collections.dataService(),
    collection: di.collections.collection(),
    router: di.collections.router(),
    user: di.collections.user(),
    facetMenu: di.collections.facetMenu(),
    facetEditor: di.collections.facetEditor(),
    facetsReorder: di.collections.facetsReorder(),
    breadcrumbs: di.collections.breadcrumbs(),
    profileEditor: di.collections.profileEditor(),
    batchEditor: di.collections.batchEditor(),
    results: di.collections.results(),
    modal: di.collections.modal(),
    resourceEditor: di.collections.resourceEditor(),
    searchForm: di.collections.searchForm(),
    pagination: di.collections.pagination(),
    channelSearch: di.shared.channelSearch()
  };
};
