di.collections.facetEditor = function () {
  var viewModel;
  var facets = ko.observableArray();
  var selectedFacet = ko.observable();
  var modalPage = ko.observable('index');
  var facetToAdd = ko.observable();
  
  var init = function (_viewModel) {
    viewModel = _viewModel;
  };
  
  var show = function () {
    showIndex();
    facets(viewModel.facetMenu.facets().slice(0));
    viewModel.modal.show('.facet-editor-modal');
  };
  
  var hide = function () {
    viewModel.modal.hide('.facet-editor-modal');
  };
  
  var selectFacet = function (facet) {
    selectedFacet(facet);
    showEdit();
    $('#facet-to-rename').val(facet.name);
  };
  
  var showIndex = function () {
    modalPage('index');
  };
  
  var showEdit = function (name) {
    modalPage('edit');
  };
  
  var showNew = function () {
    facetToAdd('');
    modalPage('new');
    $('#facet-to-add').focus();
  };
  
  var showReorder = function () {
    viewModel.facetsReorder.show();
    modalPage('reorder');
  };
  
  var cancelNew = function () {
    facets().length ? showIndex() : hide();
  };
  
  var addFacet = function () {
    if (facetToAdd()) {
      $.post('/add_facet.json', {collectionId: di.collectionData.id, name: facetToAdd()}, function (response) {
        if (response.error) {
          $('#facet-to-add').addClass('input-error');
          $('#add-facet-error').show();
        } else {
          facets.push({id: response.facet_id, name: facetToAdd(), values: [], valueCount: 0});
          $.publish('facetsEdit', [facets()]);
          hide();
        }
      });
    }
  };
  
  var renameFacet = function () {
    var currentName = selectedFacet().name;
    var newName = $('#facet-to-rename').val();
    if (false && newName === currentName) {
      showIndex();
    } else if ($.trim(newName) !== '') {
      var data = {id: selectedFacet().id, name: newName};
      $.post('/facets/update.json', data, function (error) {
        if (error) {
          $('#facet-to-rename').addClass('input-error');
          $('#rename-facet-error').show();
        } else {
          selectedFacet().name = newName;
          $.publish('facetsEdit', [facets()]);
          hide();
        }
      });
    }
  };

  var showDeleteConfirmationModal = function () {
    selectedFacet(this);
    viewModel.modal.show('.delete-modal');
  };

  var hideDeleteConfirmationModal = function () {
    $('.delete-modal .waiting').hide();
    viewModel.modal.hide('.delete-modal');
  };
  
  var deleteFacet = function (facet) {
    $('.delete-modal .waiting').show();
    $.post('/delete_facet.json', {id: selectedFacet().id}, function (data) {
      facets.remove(selectedFacet());
      $.publish('facetsEdit', [facets()]);
      hideDeleteConfirmationModal();
      hide();
    });
  };

  return {
    init: init,
    facets: facets,
    selectedFacet: selectedFacet,
    facetToAdd: facetToAdd,
    modalPage: modalPage,
    show: show,
    showIndex: showIndex,
    showNew: showNew,
    showReorder: showReorder,
    showDeleteConfirmationModal: showDeleteConfirmationModal,
    hideDeleteConfirmationModal: hideDeleteConfirmationModal,
    hide: hide,
    selectFacet: selectFacet,
    addFacet: addFacet,
    cancelNew: cancelNew,
    addFacet: addFacet,
    renameFacet: renameFacet,
    deleteFacet: deleteFacet
  };
};
