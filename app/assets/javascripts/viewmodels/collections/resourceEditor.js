di.collections.resourceEditor = function () {
  var viewModel;
  var resource = ko.observable();
  
  var init = function (_viewModel) {
    viewModel = _viewModel;
    $('.metadata-modal').on('keydown', 'input', function (e) {
      var code = (e.keyCode ? e.keyCode : e.which);
      var value = $(this).val();
      // Keys: comma (188), enter (13), tab (9)
      if ((code === 188 || code === 13 || code === 9) && value !== '') {
        var facet = ko.dataFor(this);
        addValue(value, facet);      
        $(this).val('');
        return false;
      } else if (code === 8 && value === '') { // key: backspace (8)
        var facet = ko.dataFor(this);
        var value = facet.values()[facet.values().length -1];
        if (value) {
          removeValue(value, facet);
          $(this).val(value.name).select();
          return false;
        }
      }
    });
    $('.metadata-modal').on('mousedown', '.modal-body', function (e) {
      $('.metadata-modal input').not(e.target).each(function () {
        e = $.Event( "keydown", { keyCode: 188 } );
        $(this).trigger(e);
      });
    });
  };
  
  var edit = function (_resource) {
    _.each(_resource.facets(), function (facet) {
      _.each(facet.values, function (value) {
        value.isRemoved = ko.observable(false);
      });
      facet.values = ko.observableArray(facet.values);
      facet.ishere = true;
    });
    resource(_resource);
    show();
  };
  
  var addValue = function (valueName, facet) {
    var existingValue = _.find(facet.values(), function (value) {
      return value.name === valueName && (value.isAdded || !value.isRemoved());      
    });
    if (!existingValue) {
      var newValue = {name: valueName};
      newValue.isAdded = true;
      facet.values.push(newValue);
    }
  };

  var removeValue = function (value, facet) {
    if (value.isRemoved) {
      var existingValue = _.find(facet.values(), {name: value.name, isAdded: true});
      if (!existingValue) {
        value.isRemoved(!value.isRemoved());
      }
    } else {
      facet.values.remove(value);
    }
    $('a').blur();
  };
  
  var show = function () {
    viewModel.modal.show('.metadata-modal', close);
    var $inputs = $('.metadata-modal input');
    if ($inputs.length) {
      $inputs.each(function () {
        var facet = ko.dataFor(this);
        var facetValues = viewModel.collection.getFacets().get(facet.id).values;
        $(this).typeahead({
          source: _.pluck(facetValues, 'name'),
          updater: function (value) { addValue(value, facet); }
        });
      });
      $inputs[0].focus();
    }
  };

  var close = function () {
    restoreResource();
    $('#loading-gif').insertBefore('.metadata-modal').hide();
    viewModel.modal.hide();
    resource(null);
  };
  
  var save = function () {
    var changes = [];
    _.each(resource().facets(), function (facet) {
      _.each(facet.values(), function (value) {
        if (value.isAdded) {
          changes.push({name: value.name, facet: facet.name});
        } else if (value.isRemoved()) {
          changes.push(value);
        }
      });
    });
    if (changes.length) {
      $('#save-button').removeClass('btn-primary').prop('disabled', true);
      $('#resource-editor-cancel-button').hide();
      $('#loading-gif').insertAfter('#save-button').show();
      saveResource(changes);
    } else {
      close();
    }
  };
  
  var saveResource = function (changes) {
    var _resource = resource();
    var values = _.map(changes, function (change) {
      return change.id ? {id: change.id} : change;
    });
    var data = {
      resource_id: _resource.id,
      values: values,
      collection_id: di.collectionData.id
    }; 
    $.post('/resources/edit.json', data, function(data) {
      close();
      $.publish('valuesEdit', [data.added, data.removed, data.created, data.deleted]);
      $.publish('resourceEdit', [_resource, data.added, data.removed]);
      $.publish('outOfSyncChange', [true]);
    });
  };
  
  var restoreResource = function () {
    _.each(resource().facets(), function (facet) {
      facet.values = _.filter(facet.values(), function (value) {
        delete value._destroy;
        return typeof value.id !== 'undefined';
      });
    });
  };

  return {
    init: init,
    edit: edit,
    resource: resource,
    close: close,
    save: save,
    removeValue: removeValue
  };
};
