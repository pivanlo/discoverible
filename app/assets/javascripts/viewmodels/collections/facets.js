di.collections.facets = function () {
  var facets;
  
  var init = function (_facets) {
    facets = _facets;
    $.subscribe('valuesUpdate', update);
    $.subscribe('valuesCountUpdate', updateValueCounts);
    $.subscribe('facetsEdit', updateFacets);
  };
  
  var updateFacets = function (_facets) {
    facets = _facets;
    $.publish('facetsUpdate', [facets]);
  };
  
  var update = function (values) {
    _.each(facets, function (facet) {
      facet.values = _.filter(values, {facetId: facet.id});
    });
    updateValueCounts();
  };
  
  var updateValueCounts = function () {
    _.each(facets, function (facet) {
      facet.valueCount = _.filter(facet.values, function (value) {
        return value.count > 0 && !value.isSelected(); 
      }).length;
    });
    $.publish('facetsValueCountUpdate', [facets]);
  };
  
  // Returns an array of all facets.
  // values: Optional array of values to filter by.
  var getAll = function (values) {
    if (values) {
      var valuesByFacetId = _.groupBy(values, 'facetId');
      return _.map(facets, function (facet) {
        return {
          id: facet.id,
          name: facet.name,
          values: valuesByFacetId[facet.id] || []
        };
      });
    }
    return facets;
  };
  
  var get = function (id) {
    return _.find(facets, {'id': id});
  };
  
  return {
    init: init,
    getAll: getAll,
    get: get,
    updateValueCounts: updateValueCounts
  };
};
