di.collections.profileEditor = function () {
  var viewModel;
  var currentTitle, currentImageUrl;
  
  var init = function (_viewModel) {
    viewModel = _viewModel;
    currentTitle = di.collectionData.title;
    currentImageUrl = di.collectionData.image_url;
  };
  
  var show = function () {
    $('#profile-title-input').val(currentTitle);
    $('#profile-image-url-input').val(currentImageUrl);
    viewModel.modal.show('.profile-modal');
  };
  
  var close = function () {
    viewModel.modal.hide();
  };
  
  var save = function () {
    var data = {
      id: di.collectionData.id,
      title: $('#profile-title-input').val(),
      imageUrl: $('#profile-image-url-input').val()
    };
    if (data.title !== currentTitle || data.imageUrl !== currentImageUrl) {
      $.post('/collections/update.json', data, function () {
        $('#content-heading').text(data.title);
        $('#collection-image').attr('src', data.imageUrl);
        currentTitle = data.title;
        currentImageUrl = data.imageUrl;
        viewModel.modal.hide();
      });
    }
  };  
  
  return {
    init: init,
    show: show,
    close: close,
    save: save
  };
};
