di.collections.pagination = function () {
  var pages = ko.observableArray();
  var currentPage = ko.observable();
  var totalPages = ko.observable();

  var init = function () {
    $.subscribe('resultsReady', update);
  };
  
  var update = function (data, page) {
    totalPages(Math.ceil(data.count / 10));
    var range = _.range(page - 7, page + 7);
    range = _.reject(range, function (page) {
      return page < 1 || page > totalPages();
    });
    while (range.length > 7) {
      (page - _.first(range) > _.last(range) - page) ? range.shift() : range.pop();
    }
    pages(range);
    pages.refresh();
    currentPage(page);
  };
  
  var urlForPage = function (page) {
    var hash = (location.href.split("#")[1] || "");
    if (hash) {
      hash = '#' + hash.replace(/^\/\d+/, '/' + page);
    } else {
      hash = '#/' + page; 
    }
    return hash;
  };
  
  var update2 = function (data, offset) {
    $('.previous-page, .next-page').show();
    $('.previous-page-disabled, .next-page-disabled').hide();
    var currentHash = '#' + (location.href.split("#")[1] || "");
    var nextPageHash, previousPageHash;
    if (currentHash === '#') {
      nextPageHash = '#/10';
    } else {
      nextPageHash = currentHash.replace('#/' + offset, '#/' + (offset + 10));
      previousPageHash = currentHash.replace('#/' + offset, '#/' + Math.max(0, offset - 10));
    }
    $('.next-page').attr('href', nextPageHash);
    $('.previous-page').attr('href', previousPageHash);
    if (offset === 0) {
      $('.previous-page').hide();
      $('.previous-page-disabled').show();
    }
    if (offset + 11 > data.count) {
      $('.next-page').hide();
      $('.next-page-disabled').show();
    }
  };

  return {
    pages: pages,
    currentPage: currentPage,
    totalPages: totalPages,
    init: init,
    update: update,
    urlForPage: urlForPage
  };
};











