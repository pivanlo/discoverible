di.collections.mediator = function () { 
  var viewModel;
  
  var init = function (_viewModel) {
    viewModel = _viewModel;
  };
  
  var getValueById = function (id) {
    var value;
    _.each(viewModel.collection.facets, function (facet) {
      value = _.find(facet.values, {id: id});
      if (value) {
        return false;
      }
    });
    return value || null;
  };
  
  var getValueByFacetAndValueNames = function (facetName, valueName) {
    var facet = _.find(viewModel.collection.getFacets(), {name: facetName});
    return _.find(facet.values, {name: valueName});
  };

  return {
    init: init
    //getValueById: getValueById,
    //getValueByFacetAndValueNames: getValueByFacetAndValueNames
  };
};










