di.collections.batchEditor = function () {
  var viewModel;
  var facets = ko.observableArray();
  var selectedFacet = ko.observable();
  var modalView = ko.observable('index');
  
  var init = function (_viewModel) {
    viewModel = _viewModel;
  };
  
  var show = function () {
    showIndexView();
    facets(viewModel.collection.getFacets().getAll());
    viewModel.modal.show('.batch-editor-modal');
  };

  var showIndexView = function () {
    modalView('index');
  };

  var showAddView = function () {
    selectedFacet(this);
    modalView('add');
    var values = selectedFacet().values;
    $('#batch-add-input')
      .typeahead({source: _.pluck(values, 'name')})
      .focus();
  };

  var showRemoveView = function () {
    selectedFacet(this);
    modalView('remove');
    var values = _.reject(selectedFacet().values, {count: 0});
    $('#batch-remove-input')
      .typeahead({source: _.pluck(values, 'name')})
      .focus();
  };
  
  var showEmptyConfirmationModal = function () {
    selectedFacet(this);
    viewModel.modal.show('.empty-modal');
  };

  var hideEmptyConfirmationModal = function () {
    $('.delete-modal .waiting').hide();
    viewModel.modal.hide('.empty-modal');
  };

  var close = function () {
    $('#batch-editor-modal-waiting').insertBefore('.batch-editor-modal').hide();
    viewModel.modal.hide();
  };
  
  var addValue = function () {
    var data = {
      name: $('#batch-add-input').val(),
      facetId: selectedFacet().id
    };
    saveChanges([data]);
  };
  
  var removeValue = function () {
    var valueName = $('#batch-remove-input').val();
    if (valueName) {
      var value = _.find(selectedFacet().values, {name: valueName});
      var data = {id: value.id};
      saveChanges([data]);
    }
  };
  
  var emptyFacet = function () {
    var values = _.reject(selectedFacet().values, {count: 0});
    if (values.length) {
      var data = _.map(values, function (value) { return {id: value.id}; });
      saveChanges(data, '.empty-modal .modal');
    } else {
      close();
    }
  };
  
  var save = function () {
    var values = [];
    _.each(facets(), function (facet) {
      _.each(facet.values(), function (value) {
        if (value.isAdded) {
          values.push({name: value.name, facet: facet.name});
        } else if (value.isRemoved()) {
          values.push({id: value.id});
        }
      });
    });
  };
  
  var saveChanges = function (values, modalSelector) {
    var data = {
      selectedValues: _.pluck(viewModel.collection.getValues().getSelected(), 'id'),
      values: values,
      collectionId: di.collectionData.id
    };
    $('#batch-editor-modal-waiting').prependTo(modalSelector || '.batch-editor-modal .modal').show();
    $.post('/resources/edit_all.json', data, function(data) {
      close();
      var resources = viewModel.results.resources();
      var count = viewModel.results.count();
      $.publish('valuesEdit', [data.added, data.removed, data.created, data.deleted, count]);
      var dataRemovedAndDeleted = data.removed.concat(data.deleted);
      _.each(resources, function (resource) {
        $.publish('resourceEdit', [resource, data.added, dataRemovedAndDeleted]);
      });
      $.publish('outOfSyncChange', [true]);
    });
  };
  
  return {
    selectedFacet: selectedFacet,
    init: init,
    show: show,
    showIndexView: showIndexView,
    showAddView: showAddView,
    showRemoveView:showRemoveView,
    showEmptyConfirmationModal: showEmptyConfirmationModal,
    hideEmptyConfirmationModal: hideEmptyConfirmationModal,
    facets: facets,
    modalView: modalView,
    addValue: addValue,
    removeValue: removeValue,
    emptyFacet: emptyFacet,
    close: close
  };
};
