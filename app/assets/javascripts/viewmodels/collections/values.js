di.collections.values = function () {
  var values, viewModel;
  
  var init = function (_viewModel, _values) {
    viewModel = _viewModel;
    values = _values;
    _.each(values, function (value) {
      value.isSelected = ko.observable();
      value.hash = ko.observable();
    });
    updateDisplayNames();
    updateHashProperty();
    $.subscribe('selectedValuesChange', updateSelectedValues);
    $.subscribe('resultsReady', updateCountProperty);
    $.subscribe('valuesEdit', editValues);
  };

  var updateDisplayNames = function () {
    _.each(values, function (value) {
      var sameNameValues = _.filter(values, {name: value.name});
      if (sameNameValues.length > 1) {
        value.displayName = value.name + ' (' + value.facetName + ')'
      } else {
        value.displayName = value.name;
      }
    });
  };
  
  var updateSelectedValues = function (data, callback) {
    var valueIds = data.split('+');
    _.each(values, function (value) {
      value.isSelected(false);
    });
    var selectedValues = [];
    _.each(valueIds, function (id) {
      var value = get(id);
      if (value) {
        value.isSelected(true);
        selectedValues.push(value);
      }
    });
    updateHashProperty();
    callback(selectedValues);
    $.publish('selectedValuesUpdate', [selectedValues]);
  };  
  
  var updateCountProperty = function (data) {
    _.each(values, function (value) {
      value.count = 0;
    });
    _.each(data.terms, function (term) {
      var value = _.find(values, {id: term.term});
      if (value) {
        value.count = term.count;
      }
    });
    viewModel.collection.getFacets().updateValueCounts();
    $.publish('valuesCountUpdate', [values]);
  };
  
  var editValues = function (valuesAddedIds, valuesRemovedIds, valuesCreated, valuesDeletedIds, resourceCount) {
    _.each(valuesCreated, function (value) {
      createValue(value.id, value.name, value.facet_id);
    });
    deleteValues(valuesDeletedIds);
    _.each(get(valuesAddedIds), function (value) {
      value.count += resourceCount || 1;
    });
    _.each(get(valuesRemovedIds), function (value) {
      if (value) {
        value.count -= resourceCount || 1;
      }
      if (value.count < 1) {
        value.count = 0;
      }
    });
    updateDisplayNames();
    $.publish('valuesUpdate', [values]);
  };
  
  var get = function () { // argument can be and id or an array of ids
    if (_.isArray(arguments[0])) {
      var returnValues = [];
      _.each(arguments[0], function (id) {
        var value = _.find(values, {id: id});
        if (value) {
          returnValues.push(value);
        }
      });
      return returnValues;
    }
    return _.find(values, {id: _.parseInt(arguments[0])});
  };
  
  var getAll = function () {
    return values;
  };
  
  var getSelected = function () {
    return _.filter(values, function (value) {
      return value.isSelected();
    });
  };
  
  var updateHashProperty = function (value) {
    var valuesToUpdate = value ? [value] : values;
    var selectedValues = getSelected();
    var currentHash = '#' + location.href.split("#")[1] || "";
    _.each(valuesToUpdate, function (value) {
      if (_.contains(selectedValues, value)) {
        value.hash(currentHash
          .replace(value.id + '+', '')
          .replace('/' + value.id, '')
          .replace('+' + value.id, ''));
      } else {
        value.hash(selectedValues.length ? currentHash + '+' + value.id : '#/1/' + value.id);
      }
    });
  };
  
  var createValue = function (id, name, facetId) {
    var facet = viewModel.collection.getFacets().get(facetId);
    var value = {
      id: id,
      name: name,
      facetId: facetId,
      facetName: facet.name,
      count: 0,
      isSelected: ko.observable(false),
      hash: ko.observable()
    };
    updateHashProperty(value);
    values.push(value);
    return value;
  };
  
  var deleteValues = function (ids) {
    values = _.reject(values, function (value) {
      return _.contains(ids, value.id);
    });
  };
  
  return {
    init: init,
    get: get,
    getAll: getAll,
    getSelected:getSelected,
    createValue: createValue,
    deleteValues: deleteValues
  };
};


















