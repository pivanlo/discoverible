di.collections.modal = function () {
  var BACKDROP_TEMPLATE = '<div class="di-modal-backdrop"></div>';
  var callback;
  
  var init = function () {
    $('body').on('click', '.di-modal-backdrop', function () {
      if ($('.di-modal-backdrop').length === 1) {
        $('.player').removeClass('player-hidden');
      }
      $(this).parent().hide();
      $(this).remove();
      callback && callback();
    });
  };
  
  var show = function (selector, _callback) {
    $('.di-modal-container').css('margin-top', $(window).scrollTop()); 
    $('.player').addClass('player-hidden');
    $(selector).show().prepend(BACKDROP_TEMPLATE);
    callback = _callback;
  };
  
  var hide = function (selector) {
    if ($('.di-modal-backdrop').length === 1) {
      $('.player').removeClass('player-hidden');
    }
    if (typeof selector === 'string') {
      $(selector).children('.di-modal-backdrop').remove();
      $(selector).hide();
    } else {
      $('.di-modal-backdrop').remove();
      $('.di-modal-container').hide();
    }
  };

  return {
    init: init,
    show: show,
    hide: hide
  };
};
