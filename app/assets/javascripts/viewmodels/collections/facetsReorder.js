di.collections.facetsReorder = function () {
  var viewModel;
  var facets = ko.observableArray();
  
  var init = function (_viewModel) {
    viewModel = _viewModel;
  };
  
  var show = function () {
    facets(viewModel.facetMenu.facets().slice(0));
  };
  
  var moveUp = function (facet) {
    var index = _.indexOf(facets(), facet);
    facets.splice(index, 1);
    facets.splice(index - 1, 0, facet);
  };
  
  var moveDown = function (facet) {
    var index = _.indexOf(facets(), facet);
    facets.splice(index, 1);
    facets.splice(index + 1, 0, facet);
  };
  
  var save = function () {
    var hasChanged = false;
    var originalFacets = viewModel.facetMenu.facets();
    _.each(facets(), function (facet, index) {
      if (facet !== originalFacets[index]) {
        hasChanged = true;
        return false;
      }
    });
    if (hasChanged) {
      var positions = _.map(facets(), function (facet, index) {
        return [facet.id, index]; 
      });
      $.post('/reorder', {positions: positions}, function () {
        $.publish('facetsEdit', [facets()]);
        viewModel.facetEditor.hide();
      });
    } else {
      viewModel.facetEditor.showIndex();
    }
  };

  return {
    init: init,
    show: show,
    facets: facets,
    moveUp: moveUp,
    moveDown: moveDown,
    save: save
  };
};











