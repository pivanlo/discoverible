di.collections.results = function () {
  var viewModel;
  var count = ko.observable();
  var isGettingResults = false;
  var resources = ko.observableArray();
  var selectedResult = ko.observable();
  
  var init = function (_viewModel) {
    viewModel = _viewModel;
    $.subscribe('resultsReady', update);
  };
  
  var update = function (data, offset) {
    _.each(data.resources, function (resource) {
      resource.playerUrl = resource.url.replace('/watch?v=', '/embed/');
      resource.values = viewModel.collection.getValues().get(resource.values);
      var facets = viewModel.collection.getFacets().getAll(resource.values);
      resource.facets = (viewModel.page === 'edit') ? ko.observableArray(facets) : facets;
    });
    resources(data.resources);
    count(data.count);
  };
  
  var selectResult = function (result, e) {
    if ($(e.target).is('.resource-title a, .edit-action')) {
      return true;
    } else if (result === selectedResult()) {
      selectedResult(null);
    } else {
      selectedResult(result);
    }
  };
  
  var editResource = function (resource, e) {
    viewModel.resourceEditor.edit(resource);
  };
  
  var getValueList = function (values) {
    return _.pluck(values, 'name').join(', ');
  };

  return {
    count: count,
    init: init,
    update: update,
    resources: resources,
    selectedResult: selectedResult,
    selectResult: selectResult,
    editResource: editResource,
    getValueList: getValueList
  };
};
