di.collections.user = function () {
  var viewModel;
  var user = ko.observable();
  var email = ko.observable('');
  var password = ko.observable('');
  var username = ko.observable('');
  var isError = ko.observable(false);
  
  var init = function (_viewModel) {
    viewModel = _viewModel;
    if (di.userData) {
      user(di.userData);
      username(di.userData.username);
    }

    /*
    $('#email-input').keyup(function () {
      $(this).removeClass('input-error');
      $(this).next().hide();
      $(this).next().next().hide();
    });

    $('#password-input').keyup(function () {
      if ($(this).val().length >= 6) {
        $(this).removeClass('input-error');
        $(this).next().hide();
      }
    });

    $('#username-input').keyup(function () {
      $(this).removeClass('input-error');
      $(this).next().next().next().hide();
      $(this).next().next().next().next().hide();
      username($(this).val());
    });
    */
  };
  
  var showSignIn = function () {
    viewModel.modal.show('.signin-modal');
  };
  
  var hideSignIn = function () {
    viewModel.modal.hide('.signin-modal');
    isError(false);
  };

  var signUp = function () {
    var queryString = 'email=' + email() + '&password=' + password() + '&username=' + username();
   $.get('/users/create?' + queryString, function(response) {
      if (response.id) {
        window.location.replace('/' + response.username);
      } else {
        if (response.email && (response.email[0] === "can't be blank" || response.email[0] === "is not an email") ) {
          $('#email-input')
            .addClass('input-error')
            .next().show();
        } else if (response.email && response.email[0] === "has already been taken") {
          $('#email-input')
            .addClass('input-error')
            .next().next().show();
        }
        if (response.password_digest) {
          $('#password-input')
            .addClass('input-error')
            .next().show();
        }
        if (response.username && response.username[0] === "can't be blank" ) {
          $('#username-input')
            .addClass('input-error')
            .next().next().next().show();
        } else if (response.username && response.username[0] === "has already been taken") {
          $('#username-input')
            .addClass('input-error')
            .next().next().next().next().show();
        }
      }
    });
  };

  var signIn = function () {
    var queryString = 'email=' + email() + '&password=' + password();
    $.get('/sessions/create?' + queryString, function (response) {
      
    });
  };

  var signOut = function () {
    $.get('/sessions/destroy', {collectionId: di.collectionData.id});
  };

  return {
    init: init,
    showSignIn: showSignIn,
    hideSignIn: hideSignIn,
    signUp: signUp,
    signIn: signIn,
    signOut: signOut,
    user: user,
    email: email,
    password: password,
    username: username,
    isError: isError
  };
};













