di.collections.collection = function () {
  var facets, values, resources;

  var init = function (_viewModel) {
    viewModel = _viewModel;
    viewModel.dataService.getFacets(function (data) {
      facets = di.collections.facets();
      values = di.collections.values();
      resources = di.collections.resources();
      facets.init(data);
      var valuesData = [];
      _.each(data, function (facet) {
        _.each(facet.values, function (value) {
          value.facetId = facet.id;
          value.facetName = facet.name;
        });
        valuesData = valuesData.concat(facet.values);
      });
      values.init(viewModel, valuesData);
      resources.init(viewModel);
      viewModel.router.init(viewModel, values);
      viewModel.searchForm.init(valuesData);
    });
  };
  
  var getFacets = function () {
    return facets;
  };
  
  var getValues = function () {
    return values;
  };
  
  var getResources = function () {
    return resources;
  };
  
  var getValueByName = function (name) {
    var value;
    _.each(facets, function (facet) {
      value = _.find(facet.values, function (value) {
        return value.displayName === name || value.name === name;
      });
      if (value) {
        return false;
      }
    });
    return value || null;
  };
   
  return {
    init: init,
    getFacets: getFacets,
    getValues: getValues,
    getResources: getResources
  };
};










