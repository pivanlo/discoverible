di.collections.searchForm = function () {
  var init = function (values) {
    $('#search').typeahead();
    update(values);
    $.subscribe('valuesUpdate', update);
  };
  
  var update = function (values) {
    var hashes = {};
    var valueNames = [];
    _.each(values, function (value) {
      var name = value.displayName;
      valueNames.push(name);
      hashes[name] = '#/1/' + value.id;
    });
    $('#search').data('typeahead').source = valueNames;
    $('#search').data('typeahead').updater = function (name) {
      window.location.hash = hashes[name];
    };
  };

  return {
    init: init
  };
};
