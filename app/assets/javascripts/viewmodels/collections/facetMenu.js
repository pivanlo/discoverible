di.collections.facetMenu = function () {
  var viewModel;
  var hasFacets = ko.observable(true);
  var listLength = ko.observable(40);
  var facets = ko.observableArray();
  var selectedFacet = ko.observable();
  var sortCriteria = ko.observable('Count');
  
  var init = function (_viewModel) {
    viewModel = _viewModel;
    selectedFacet.subscribe(function () {
      listLength(40);
    });
    $.subscribe('facetsValueCountUpdate', update);
    $.subscribe('facetsUpdate', update);
  };
  
  var update = function (_facets) {
    facets(_facets);
    sort(sortCriteria());
    refresh();
    _facets.length ? hasFacets(true) : hasFacets(false);
  };
    
  var update2 = function (filters) {
    facets(_facets);
    sort(sortCriteria());
    if (selectedFacet()) {
      selectedFacet(_.find(_facets, {name: selectedFacet().name}));
      if (sortCriteria() === 'Name') {
        sort('Name');
      }
    }
    refresh();
  };
  
  var refresh = function () {
    if (selectedFacet()) {
      var _selectedFacet = selectedFacet();
      selectedFacet(null);
      selectedFacet(_selectedFacet);
    } else {
      facets.refresh();
    }
  };
  
  var sort = function (criteria) {
    _.each(facets(), function (facet) {
      viewModel.facetMenu['sortBy' + criteria](facet.values);
    });
    sortCriteria(criteria);
    selectedFacet.valueHasMutated();
  };
  
  var sortByCount = function (values) {
    values.sort(function (left, right) {
      if (left.count === right.count) {
        return (left.name < right.name) ? -1 : (left.name > right.name) ? 1 : 0;
      }
      return (left.count < right.count) ? 1 : -1;
    });
  };
  
  var sortByName = function (values) {
    var valuesToSort = [];
    var otherValues = [];
    _.each(values, function (value) {
      value.count > 0 ? valuesToSort.push(value) : otherValues.push(value);
    });
    valuesToSort.sort(function (left, right) {
      return (left.name < right.name) ? -1 : (left.name > right.name) ? 1 : 0;
    });
    values.splice(0);
    values.push.apply(values, valuesToSort);
    values.push.apply(values, otherValues);
  };
  
  var showAll = function () {
    if (listLength() < selectedFacet().valueCount) {
      listLength(listLength() + 50);
      setTimeout(function () {
        showAll();
      }, 20);
    }
  };

  var newFacet = function () {
    viewModel.facetEditor.show();
    viewModel.facetEditor.showNew();
  };

  return {
    hasFacets: hasFacets,
    init: init,
    update: update,
    listLength: listLength,
    facets: facets,
    selectedFacet: selectedFacet,
    sortCriteria: sortCriteria,
    sort: sort,
    sortByCount: sortByCount,
    sortByName: sortByName,
    refresh: refresh,
    showAll: showAll,
    newFacet: newFacet
  };
};











