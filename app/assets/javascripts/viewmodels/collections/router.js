di.collections.router = function () {
  var viewModel;

  var init = function (_viewModel) {
    viewModel = _viewModel;
    Path.map('#(/:offset)(/)(:values)').to(function () {
      var offset = Math.max(0, _.parseInt(this.params.offset) || 0);
      var values = location.href.split("#")[1].split('/')[2] || ''; // see comment at the bottom
      $.publish('pathChange', [offset, values]);
    });
    Path.listen();
    if (location.hash === '' || location.hash === '#' || location.hash === '#/') {
      $.publish('pathChange', [1, '']);
    }
  };
  
  return {
    init: init
  };
};

// Firefox automatically decodes encoded parameters in URL.
// This is a problem when a facet value includes the character '+'.
// http://www.quora.com/Does-window-location-hash-work-across-all-browsers
// http://stackoverflow.com/q/4835784
