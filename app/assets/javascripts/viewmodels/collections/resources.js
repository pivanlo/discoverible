di.collections.resources = function () {
  var viewModel;
  
  var init = function (_viewModel) {
    viewModel = _viewModel;
    $.subscribe('resourceEdit', updateResource);
    $.subscribe('facetsUpdate', updateFacets);
  };
  
  var updateFacets = function () {
    var resources = viewModel.results.resources();
    _.each(resources, function (resource) {
      resource.facets(viewModel.collection.getFacets().getAll(resource.values));
    });
  };
  
  var updateResource = function (resource, valuesAddedIds, valuesRemovedIds) {
    var valuesAdded = viewModel.collection.getValues().get(valuesAddedIds);
    _.each(resource.facets(), function (facet) {
      facet.values = _.reject(facet.values, function (value) {
        return _.contains(valuesRemovedIds, value.id);
      });
      facet.values = facet.values.concat(_.filter(valuesAdded, {facetId: facet.id}));
    });
    resource.facets.refresh();
  };
  
  return {
    init: init,
    updateResource: updateResource
  };
};
