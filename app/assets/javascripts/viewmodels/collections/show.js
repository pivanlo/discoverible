di.collections.show = function () {
  var init = function () {
    this.collection.init(this);
    this.facetMenu.init(this);
    this.user.init(this);
    this.modal.init();
    this.dataService.init(this);
    this.results.init(this);
    this.mediator.init(this);
    this.pagination.init();
    this.breadcrumbs.init();
    this.channelSearch.init(this);
    window.vm = this; // for debugging
    return this;
  };
  
  return {
    init: init,
    dataService : di.collections.dataService(),
    collection: di.collections.collection(),
    router: di.collections.router(),
    user: di.collections.user(),
    facetMenu: di.collections.facetMenu(),
    breadcrumbs: di.collections.breadcrumbs(),
    results: di.collections.results(),
    modal: di.collections.modal(),
    searchForm: di.collections.searchForm(),
    pagination: di.collections.pagination(),
    mediator: di.collections.mediator(),
    channelSearch: di.shared.channelSearch()
  };
};
