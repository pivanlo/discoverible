di.home.index = function () {
  var init = function () {
    this.channelSearch.init(this);
    return this;
  };

  var signIn = function () {
    var url = 'https://accounts.google.com/o/oauth2/auth?' + 
      'client_id=766646820376-frp8mt3b17bts1he265j2uvstvl3jv9i.apps.googleusercontent.com' + 
      '&redirect_uri=http://www.discoverible.com/sessions/create' + 
      '&scope=https://www.googleapis.com/auth/youtube.readonly' + 
      '&response_type=code' + 
      '&access_type=offline';
    window.document.location = url;
  };

  return {
    init: init,
    signIn: signIn,
    channelSearch: di.shared.channelSearch(),
  };
};
