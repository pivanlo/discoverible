di.shared.channelSearch = function () {
  var channels;

  var init = function () {
    $.get('/collections/typeahead', function (data) {
      channels = data.channels;
      $('#channel-search').typeahead({
        source: _.pluck(channels, 'title'),
        updater: function (title) {
          window.location = '/youtube/' + _.find(channels, {title: title}).channel_id
        }
      });
    });
  };

  return {
    init: init
  };
};