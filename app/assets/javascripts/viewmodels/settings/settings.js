var di = di || {};

di.settings = (function () {
  var email = ko.observable();
  var username = ko.observable();
  var currentPassword = ko.observable();
  var newPassword = ko.observable();
  var passwordConfirmation = ko.observable();
  var timer;

  var init = function () {
    if (di.userData) {
      email(di.userData.email);
      username(di.userData.username);
    }
    
    $('#email-input').keyup(function () {
      if ($(this).val() !== email()) {
        $(this).removeClass('input-error');
        $('#invalid-email-error').hide();
        $('#taken-email-error').hide();
      }
    });
    
    $('#username-input').keyup(function () {
      var newValue = $(this).val();
      if (newValue !== username()) {
        $(this).removeClass('input-error');
        $('#blank-username-error').hide();
        $('#taken-username-error').hide();
        username(newValue);
      }
    });
    
    $('#current-password-input').keyup(function () {
      var newValue = $(this).val();
      if (newValue.length >= 6 && newValue !== currentPassword()) {
        $(this).removeClass('input-error');
        $('#blank-current-password-error').hide();
        $('#incorrect-current-password-error').hide();
      }
    });
    
    $('#new-password-input').keyup(function () {
      var newValue = $(this).val();
      if (newValue.length >= 6 && newValue !== newPassword()) {
        $(this).removeClass('input-error');
        $('#blank-new-password-error').hide();
      }
    });
    
    $('#password-confirmation-input').keyup(function () {
      var newValue = $(this).val();
      if (newValue.length >= 6 && newValue !== passwordConfirmation()) {
        $(this).removeClass('input-error');
        $('#blank-password-confirmation-error').hide();
        $('#passwords-match-error').hide();
      }
    });
  };
  
  var saveAccount = function () {
    var data = {
      email: email(),
      username: username()
    };
    $.post('/settings/update.html', data, function(error) {
      if (error.email) {
        showEmailErrors(error.email);
      }
      if (error.username) {
        showUsernameErrors(error.username);
      } 
      if (!error.email && !error.username) {
        $('input, button').blur();
        window.scrollTo(0, 0);
        $('.alert').hide();
        $('#account-success-alert').show();
        clearTimeout(timer);
        timer = setTimeout(function () { $('#account-success-alert').fadeOut(); }, 5000);
        di.user.user(data);
      }
    });
  };
  
  var showEmailErrors = function (messages) {
    if (containsString(messages, "is not an email")) {
      $('#email-input').addClass('input-error');
      $('#invalid-email-error').show();
    } else if (containsString(messages, "has already been taken")) {
      $('#email-input').addClass('input-error');
      $('#taken-email-error').show();
    }
  };
  
  var showUsernameErrors = function (messages) {
    if (containsString(messages, "can't be blank")) {
      $('#username-input').addClass('input-error');
      $('#blank-username-error').show();
    } else if (containsString(messages, "has already been taken")) {
      $('#username-input').addClass('input-error');
      $('#taken-username-error').show();
    }
  };
  
  var savePassword = function () {
    var data = {
      current_password: currentPassword() || '',
      new_password: newPassword(),
      password_confirmation: passwordConfirmation()
    };
    if (true) {
      $.post('/settings/update.html', data, function(error) {
        if (error.password) {
          showPasswordErrors(error.password);
        } else {
          currentPassword(undefined);
          newPassword(undefined);
          passwordConfirmation(undefined);
          $('input, button').blur();
          window.scrollTo(0, 0);
          $('.alert').hide();
          $('#password-success-alert').show();
          clearTimeout(timer);
          timer = setTimeout(function () { $('#password-success-alert').hide(); }, 5000);
        }
      });
    }
  };
  
  var showPasswordErrors = function (messages) {
    if (containsString(messages, "current password can't be blank")) {
      $('#current-password-input').addClass('input-error');
      $('#blank-current-password-error').show();
    } else if (containsString(messages, "password is incorrect")) {
      $('#current-password-input').addClass('input-error');
      $('#incorrect-current-password-error').show();
    }
    if (containsString(messages, "new password can't be blank") ||
        containsString(messages, "is too short (minimum is 6 characters)")) {
      $('#new-password-input').addClass('input-error');
      $('#blank-new-password-error').show();
    } else if (containsString(messages, "doesn't match confirmation")) {
      $('#password-confirmation-input').addClass('input-error');
      $('#passwords-match-error').show();
    }
    if (containsString(messages, "password confirmation can't be blank")) {
      $('#password-confirmation-input').addClass('input-error');
      $('#blank-password-confirmation-error').show();
    }
  };
  
  var containsString = function (array, string) {
    for (var i = 0; i < array.length; i++) {
      if (array[i] === string) {
        return true;
      }
    }
    return false;
  };
  
  return {
    init: init,
    email: email,
    username: username,
    currentPassword: currentPassword,
    newPassword: newPassword,
    passwordConfirmation: passwordConfirmation,
    saveAccount: saveAccount,
    savePassword: savePassword
  };
}());













