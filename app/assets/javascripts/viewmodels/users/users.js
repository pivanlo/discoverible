var di = di || {};

di.users = (function () {
  var listItems = ko.observableArray();
  var selectedListItem = ko.observable();

  var init = function () {
    listItems(di.collectionData);
  };
  
  var selectListItem = function (listItem, e) {
    if ($(e.target).is('.resource-title a, .edit-action, .resource-snippet *')) {
      return true;
    } else if (listItem === selectedListItem()) {
      selectedListItem(null);
    } else {
      selectedListItem(listItem);
    }
  };

  return {
    init: init,
    listItems: listItems,
    selectedListItem: selectedListItem,
    selectListItem: selectListItem
  };
}());
