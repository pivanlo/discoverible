class ResultsController < ApplicationController
  def index
    collection = Collection.find(params[:collectionId])
    offset = params[:offset].to_i
    if params[:values]
      results = collection.resources.select("resources.id").joins(:values)
      results = results.where('values.id' => params[:values]).order("resources.id ASC")
      results = results.group("resources.id").having("count(*) = ?", params[:values].length)
      result_ids = results.map { |r| r.id }
      resources = collection.resources.where(id: result_ids).includes(:values).select("id")
      values = resources.map { |r| r.values }.flatten
      counts = values.each_with_object(Hash.new(0)) { |v,h| h[v.id] += 1 } # https://coderwall.com/p/layvtw
      terms = []
      counts.each { |key, value| terms << {term: key, count: value} }
      resources = collection.resources.where(id: result_ids[offset, 10]).order("resources.id ASC")
      resources = resources.includes(:values).select("id, title, url")
      count = results.length
    else
      values = Value.select("values.id, count(*)").joins(:resources).group("values.id")
      terms = values.map { |v| {term: v.id, count: v.count.to_i} }
      resources = collection.resources.order("resources.id ASC").includes(:values)
      resources = resources.limit(10).offset(offset).select("id, title, url")
      count = collection.resources.count
    end
    terms = terms.sort_by { |term| term[:count] }.reverse
    resources = resources.map do |r|
      {
        id: r.id,
        title: r.title,
        url: r.url,
        values: r.values.map { |v| v.id }
      }
    end
    out_of_sync = collection.resources.where(dirty: true).any?
    render json: {resources: resources, count: count, terms: terms, oos: out_of_sync}
  end
end
