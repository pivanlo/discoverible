class ResourcesController < ApplicationController  
  def edit
    collection = Collection.includes(facets: :values).find(params[:collection_id])
    resource = Resource.includes(:values).find(params[:resource_id])
    values_added_ids = []
    values_removed_ids = []
    values_created = []
    values_deleted_ids = []
    params[:values].values.each do |v|
      if v['name']
        facet_values = collection.facets.find_by_name(v[:facet]).values
        value = facet_values.where(name: v['name']).first_or_create
        resource.values.push(value)
        value.resources_count += 1
        value.save
        if value.resources_count == 1
          values_created.push({id: value.id, name: value.name, facet_id: value.facet.id});
        end
        values_added_ids.push(value.id);
      elsif v['id']
        value = resource.values.find_by_id(v['id'].to_i)
        if value
          resource.values.delete(value)
          value.resources_count -= 1
          if value.resources_count == 0
            values_deleted_ids.push(value.id)
            value.destroy
          else
            value.save
          end
          values_removed_ids.push(value.id)
        end
      end
    end
    resource.dirty = true
    resource.save
    render json: {
      added: values_added_ids,
      removed: values_removed_ids,
      created: values_created,
      deleted: values_deleted_ids
    }
  end
  
  def edit_all
    collection = Collection.find(params[:collectionId])
    if params[:selectedValues]
      resources = Resource
        .select("resources.id")
        .joins(:values)
        .where('values.id' => params[:selectedValues])
        .group("resources.id")
        .having("count(*) = ?", params[:selectedValues].length)
    else
      resources = Resource.select("id")
    end
    added_values = []
    removed_values = []
    created_values = []
    params[:values].values.each do |v|
      if v[:id]
        value = Value.find(v[:id])
        removed_values.push(value)
      else
        facet = Facet.find(v[:facetId])
        value = facet.values.find_by_name(v[:name])
        unless value
          value = facet.values.create(name: v[:name])
          created_values.push({
            id: value.id,
            name: value.name,
            facet_id: facet.id,
            count: resources.length
          })
        end
        added_values.push(value)
      end
    end
    deleted_value_ids = Resource.update(resources, added_values, removed_values)
    render json: {
      added: added_values.map { |v| v.id },
      removed: removed_values.map { |v| v.id },
      created: created_values,
      deleted: deleted_value_ids
    }
  end
end
