class SnippetsController < ApplicationController
  def show
    if params[:youtube_collection_id]
      show_youtube_collection
    else
      show_video
    end
  end
  
  private
  
  def show_video
    render 'video', layout: false
  end
  
  def show_youtube_collection
    id = params[:youtube_collection_id]
    youtube_collection = YoutubeCollection.find(id)
    @description = youtube_collection.description
    @image_url = youtube_collection.image_url
    render 'youtube_collection', layout: false
  end
end
