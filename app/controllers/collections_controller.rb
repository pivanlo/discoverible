class CollectionsController < ApplicationController
  def show
    @collection = Collection.find(:first, conditions: ["lower(channel_id) = ?", params[:channel_id].downcase])
    if @collection
      @title = @collection.title + ' - Discoverible'
      @is_owner = session[:channel_id] == @collection.id || session[:channel_id] == 'UCfOPTLp9PrnNE92uMsv21og'
      @user_data = {username: current_user.username, is_owner: @is_owner} if current_user
      @collection_data = {
        id: @collection.id
      }
      @collection_data[:oos] = true if @collection.resources.where(dirty: true).any?
    else
      raise ActionController::RoutingError.new('Not Found')
    end
  end

  def edit
    @collection = Collection.find(:first, :conditions => ["lower(channel_id) = ?", params[:channel_id].downcase])
    @collection_data = {
      id: @collection.id,
      name: @collection.name
    }
    @is_owner = true
    render "show"
  end
  
  def update
    collection = Collection.find(params[:id])
    collection.title = params[:title]
    new_slug = collection.title.parameterize
    collection.slug = new_slug
    collection.image_url = params[:imageUrl]
    collection.save
    render json: {redirect_url: '/' + collection.slug + '/edit'}
  end

  def typeahead
    channels = Collection.select('channel_id, title')
    render json: {channels: channels || []}
  end
end
