class SettingsController < ApplicationController
  before_filter :require_signin
  
  def index
    @title = 'Settings - Discoverible'
    @data = {
      email: current_user.email,
      username: current_user.username,
      collection: current_user.collections.first.title
    }
    @collection = current_user.collections.first
  end
  
  def upload
    icon_data = File.read(params[:icon].path)
    
    require "RMagick"
    img = Magick::Image.from_blob(icon_data).first
    icon = img.resize_to_fill(150, 150)
    
    icon_base64 = Base64.encode64(icon.to_blob)
    render :inline => icon_base64
  end
  
  def authorize_upload
    filename = current_user.username
    expiration = (Time.now + 1*60).utc.iso8601
    content_type = params[:content_type]
    
    unless %w[image/jpeg image/png image/gif].include?(content_type)
      render :json => false
      return
    end
    
    policy_document = %Q{{
      "expiration": "#{expiration}",
      "conditions": [ 
        {"bucket": "di-images"},
        {"key": "#{filename}"},
        ["starts-with", "$name", ""],
        ["starts-with", "$Filename", ""],
        {"acl": "public-read"},
        {"success_action_status": "201"},
        {"Content-Type": "#{content_type}"},
        ["content-length-range", 0, 2097152]
      ]
    }}

    aws_secret_key = 'lLhzijbON4RqKcS/zRyUcIczMT3IeJrwdBl8+k7g'

    policy = Base64.encode64(policy_document).gsub("\n","")

    signature = Base64.encode64(
    OpenSSL::HMAC.digest(
        OpenSSL::Digest::Digest.new('sha1'), 
        aws_secret_key, policy)
    ).gsub("\n","")
  
    render :json => { policy: policy, signature: signature }
  end
  
  def process_image
    require 'aws-sdk'
    AWS.config(
      :access_key_id => 'AKIAIYTJWK2QI4M2EL5A',
      :secret_access_key => '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'
    )    
    s3 = AWS::S3.new
    bucket = s3.buckets['di-images']
    image = bucket.objects['pivan']
    image_data = image.read
    
    require "RMagick"
    img = Magick::Image.from_blob(image_data).first
    processed_image = img.resize_to_fill(150, 150)
    
    image.write(processed_image.to_blob)
    image.acl = :public_read
    
    render :json => nil
  end
  
  def update
    if params['email']
      update_account(params['email'], params['username'])
    elsif params['current_password'] 
      update_password(params['current_password'], params['new_password'], params['password_confirmation'])
    end
    render json: current_user.errors.messages
  end
  

  private
  def update_account(email, username)
    current_user.email = email
    current_user.username = username
    current_user.save
    puts current_user.errors.messages
  end
  
  def update_password(current_password, new_password, password_confirmation)
    errors = []
    errors << 'password is incorrect' unless current_user.authenticate(current_password)
    errors << "current password can't be blank" if current_password == ''
    errors << "new password can't be blank" if new_password.nil?
    errors << "password confirmation can't be blank" if password_confirmation.nil?
    current_user.password = new_password
    current_user.password_confirmation = password_confirmation
    current_user.save
    errors.each { |e| current_user.errors[:password] = e }
  end

  def require_signin
    if !current_user
      respond_to do |format|
        format.js { render :js => "window.location = '/login'" }
        format.html { redirect_to '/login' }
      end
    end
  end
end













