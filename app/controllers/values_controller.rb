class ValuesController < ApplicationController 
  def update
    value = Value.find(params[:id])
    if value.name == params[:new_name]
    elsif Value.where(name: params[:new_name], facet_id: value.facet.id).exists?
      error = true
    else
      value.name = params[:new_name]
      value.save
    end
    render json: error
  end
  
  def destroy
    value = Value.find(params[:id])
    value.destroy
    render json: nil
  end
end
