def get_results
    value_ids = params[:value_ids].split(',') if params[:value_ids]
    
    if value_ids
      value_count = value_ids.count
      
      query_beginning = 'select cards_values.card_id, count(*) over() as full_count from "cards_values"
                          inner join
                          (select card_id from "cards_values" 
                          inner join "values"
                          on values.id = cards_values.value_id'

      where_clauses = " where (values.id = '#{value_ids[0]}')"
      value_ids.shift
      value_ids.each { |value_id| where_clauses << " or (values.id = '#{value_id}')" }

      query_end = " group by card_id
                      having count(*) = #{value_count}) as matches
                      on matches.card_id = cards_values.card_id
                      group by cards_values.card_id
                      order by count(*) desc
                      limit (10);"
     
      results = Card.find_by_sql(query_beginning + where_clauses + query_end)
      card_ids = results.collect { |card| card.card_id }

      full_count = results[0] ? results[0].full_count : 0


      cards = Card.includes(:values).find(card_ids)
    else
      full_count = 50
      cards = Card.includes(:values).limit(full_count)
    end

    cards.each { |c| c[:values] = c.values }

    value_arrays = []
    cards.each { |card| value_arrays << card.values }
    all_values = value_arrays.flatten
    all_values_counted = all_values.inject(Hash.new(0)) { |h, e| h[e.id] += 1 ; h }
    counts_by_value = all_values_counted.map { |i| Hash['id', i[0], 'count', i[1]] }

    render :json => { :cards => cards, :countsByValue => counts_by_value }
  end