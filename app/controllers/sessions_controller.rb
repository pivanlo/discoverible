class SessionsController < ApplicationController
  def create
    require 'net/http'
    require 'net/https'
    uri = URI.parse("https://accounts.google.com/o/oauth2/token")
    https = Net::HTTP.new(uri.host, uri.port)
    https.use_ssl = true
    https.verify_mode = OpenSSL::SSL::VERIFY_NONE
    req = Net::HTTP::Post.new(uri.path)
    req['Content-Type'] = 'application/x-www-form-urlencoded'
    req.set_form_data({
      code: params[:code],
      client_id: '766646820376-frp8mt3b17bts1he265j2uvstvl3jv9i.apps.googleusercontent.com',
      client_secret: 'vHj91i4kSHvJm3JQa4zIPADc',
      redirect_uri: 'http://www.discoverible.com/sessions/create',
      grant_type: 'authorization_code'
    })
    tokens_json = https.request(req).body
    tokens = JSON.parse(tokens_json)
    access_token = tokens['access_token']
    uri = URI.parse("https://www.googleapis.com/youtube/v3/channels")
    params = {
      part: 'id,snippet',
      mine: 'true',
      key: 'AIzaSyDcrmLap_wDUMYL3yx5htrFwHgqKv-EhwE'
    }
    uri.query = URI.encode_www_form(params)
    https = Net::HTTP.new(uri.host, uri.port)
    https.use_ssl = true
    https.verify_mode = OpenSSL::SSL::VERIFY_NONE
    req = Net::HTTP::Get.new(uri.request_uri)
    req['Authorization'] = 'Bearer ' + access_token
    channels_json = https.request(req).body
    channels = JSON.parse(channels_json)
    channel_id = channels['items'][0]['id']
    title = channels['items'][0]['snippet']['title']
    thumbnail = channels['items'][0]['snippet']['thumbnails']['default']['url']
    #collection = Collection.create(channel_id: channel_id, title: title, thumbnail: thumbnail)
    #Collection.delay.import(collection)
    session[:channel_id] = channel_id
    redirect_to '/'
    #redirect_to controller: 'collections', action: 'show', channel_id: channel_id
  end

  def destroy
    channel_id = session[:channel_id]
    session[:channel_id] = nil
    redirect_to controller: 'collections', action: 'show', channel_id: channel_id
  end
end
