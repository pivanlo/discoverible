class UsersController < ApplicationController
  def index
    owner = User.find(:first, :conditions => ["lower(username) = ?", params[:username].downcase])
    if owner
      @owner = { username: owner.username }
      @user_data = {username: current_user.username, isOwner: @is_owner} if current_user
      @collections = owner.collections
      @humanized_type = {
        'Video' => 'video',
        'YoutubeCollection' => 'YouTube collection'
      }
    else
      raise ActionController::RoutingError.new('Not Found')
    end
  end
  
  def show
    owner = User.find(:first, :conditions => ["lower(username) = ?", params[:username].downcase])
    if owner
      @is_owner = (owner == current_user)
      @owner = { username: owner.username, image_url: owner.image_url }
      @user_data = {username: current_user.username, isOwner: @is_owner} if current_user
      @collections_data = owner.collections.map do |collection|
        facet_list = collection.facets.select('name').map { |f| f.name }.join(', ')
        {
          url: '/' + owner.username + '/' + collection.slug,
          title: collection.title,
          description: collection.description,
          image_url: collection.image_url,
          # resources_description: "<strong>#{collection.resources.count}</strong>" + ' item'.pluralize(collection.resources.count),
          resources_description: '<strong>Items: </strong>' + collection.resources.count.to_s,
          facets_description: collection.facets.any? ? '<strong>Facets: </strong>' + view_context.sanitize(facet_list) : '<strong>Facets: </strong><span class="muted">None</span>'
        }
      end
    else
      raise ActionController::RoutingError.new('Not Found')
    end
  end
  
  def new
    redirect_to '/' + current_user.username if current_user
  end

  def create
    @user = User.new(email: params[:email], password: params[:password], username: params[:username])
    if @user.save
      session[:user_id] = @user.id
      render :json => @user
    else
      render :json => @user.errors.messages
    end
  end
end
