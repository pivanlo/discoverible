class FacetsController < ApplicationController
  def index
    collection = Collection.find(params[:id])
    facets = collection.facets.order('position').includes(:values)
    facets = facets.map do |f|
      values = f.values.map { |v| {id: v.id, name: v.name, count: v.resources_count} }
      {id: f.id, name: f.name, values: values}
    end
    render json: facets
  end
  
  def create
    collection = Collection.find(params[:collectionId])
    if collection.facets.where("name = '#{params[:name]}'").exists?
      error = true
    else
      new_facet = Facet.new(name: params[:name], position: collection.facets.count)
      new_facet.collection = collection
      new_facet.save
      new_facet_id = new_facet.id
    end
    render json: {error: error, facet_id: new_facet_id}
  end
  
  def update
    facet = Facet.find(params['id'])
    collection = facet.collection
    if facet.name == params[:name]
    elsif collection.facets.where("name = '#{params[:name]}'").exists?
      error = true
    else
      facet.name = params[:name]
      facet.save
    end
    render json: error
  end
  
  def destroy
    Facet.destroy(params[:id])
    render json: nil
  end
  
  def reorder
    params[:positions].values.each do |p|
      Facet.update(p[0], position: p[1])
    end
    render json: nil
  end  
end










