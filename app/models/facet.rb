class Facet < ActiveRecord::Base
  attr_accessible :name, :position
  belongs_to :collection
  has_many :values, order: 'resources_count DESC, name', dependent: :destroy
end
