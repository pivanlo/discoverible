class ElasticSearch
  def ElasticSearch.add_collection(collection)
    self.initialize_tire
    resources = collection.resources.includes(:values)
    documents = []
    resources.each do |r|
      documents.push({
        id: r.id,
        collection_id: collection.id,
        url: r.url,
        title: r.title,
        player_url: r.player_url,
        values: r.values.map { |v| v.id }
      })
    end
    Tire.index 'documents' do
      import documents
      refresh
    end
  end
  
  def ElasticSearch.add_resource(data)
    self.initialize_tire
    Tire.index 'documents' do
      store(data)
      refresh
    end
  end
  
  def ElasticSearch.delete_collection(collection)
    self.initialize_tire
    index = Tire.index('documents')
    Tire::Configuration.client.delete "#{index.url}/_query?" + "q=collection_id:#{collection.id}"
  end
  
  private
  
  def ElasticSearch.initialize_tire
    require 'tire'
    Tire.configure do
      url "http://name1:password1@api.discoverible.com"
    end
  end
end
