class Resource < ActiveRecord::Base
  attr_accessible :url, :title, :dirty
  belongs_to :collection
  has_and_belongs_to_many :values, order: 'id'
  before_destroy :delete_values
  
  def Resource.update(resources, added_values, removed_values)
    deleted_value_ids = []
    resources = Array(resources)
    resource_ids = resources.map { |r| r.id }
    remove_affected_resource_ids = []
    add_affected_resource_ids = []
    connection = ActiveRecord::Base.connection
    if removed_values.any?
      remove_affected_resource_ids = removed_values.map do |v| 
        v.resources.select('resources.id').map { |r| r.id }
      end
      remove_affected_resource_ids = remove_affected_resource_ids.flatten! & resource_ids
      remove_affected_resource_id_string = remove_affected_resource_ids.join(',')
      removed_value_id_string = removed_values.map { |v| v.id }.join(',')
      connection.execute "delete from resources_values 
        where resource_id in (#{remove_affected_resource_id_string}) 
        and value_id in (#{removed_value_id_string})"
      removed_values.each do |v|
        if (v.resource_ids - remove_affected_resource_ids).empty?
          deleted_value_ids.push(v.id)
          v.delete
          removed_values.delete v
        end
      end
    end
    if added_values.any?
      insert_values = []
      added_values.each do |value|
        value_resource_ids = value.resources.select('resources.id').map { |r| r.id }
        resources.each do |resource|
          unless value_resource_ids.include?(resource.id)
            add_affected_resource_ids.push(resource.id)
            insert_values << "(" + resource.id.to_s + "," + value.id.to_s + ")"
          end
        end
      end
      connection.execute "insert into resources_values (resource_id, value_id) values #{insert_values.join(",")}"
    end
    dirty_resource_ids = remove_affected_resource_ids + add_affected_resource_ids
    dirty_resource_id_string = dirty_resource_ids.join(',')
    connection.execute "update resources set dirty = 't' where id in (#{dirty_resource_id_string})"
    (added_values + removed_values).each { |v| v.update_resources_count }
    return deleted_value_ids
  end
  
  private
  
  def delete_values
    value_ids = []
    values.delete_all.each do |value|
      value.resources_count -= 1
      value.resources_count == 0 ? value_ids.push(value.id) : value.save
    end
    Value.delete(value_ids) if value_ids.any?
  end
end
