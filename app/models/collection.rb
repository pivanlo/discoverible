class Collection < ActiveRecord::Base
  attr_accessible :channel_id, :title, :thumbnail
  has_many :facets, order: 'position', dependent: :destroy
  has_many :resources, dependent: :destroy
  
  def import
    require 'time'
    year_facet = Facet.new(name: 'Year', position: 0)
    year_facet.collection = self
    year_facet.save
    developer_key = 'AIzaSyDcrmLap_wDUMYL3yx5htrFwHgqKv-EhwE'
    client = Google::APIClient.new(key: developer_key, authorization: nil)
    youtube = client.discovered_api('youtube', 'v3')
    channels_response = client.execute!(
      api_method: youtube.channels.list,
      parameters: {
        id: self.channel_id,
        part: 'contentDetails'
      }
    )
    uploads_list_id = channels_response.data.items[0].contentDetails.relatedPlaylists.uploads
    page_token = ''
    year_data = []
    begin
      items_response = client.execute!(
        api_method: youtube.playlist_items.list,
        parameters: {
          playlistId: uploads_list_id,
          part: 'snippet',
          maxResults: 50,
          pageToken: page_token
        }
      )
      resources = []
      items_response.data.items.each do |item|
        resource = Resource.new(
          url: "http://www.youtube.com/watch?v=#{item.snippet.resourceId.videoId}",
          title: item.snippet.title,
          dirty: true
        )
        resource.collection = self
        year = item.snippet.publishedAt.year
        year_value = year_facet.values.where(name: year.to_s).first_or_create
        year_data << [resource.url, year_value]
        resources << resource
      end
      Resource.import resources
      resources = self.resources.order('id desc').limit(resources.length)
      inserts = []
      year_data.each do |d|
        resource = resources.detect { |r| r.url == d[0] }
        value = d[1]
        inserts << "('#{resource.id}','#{value.id}')" if resource && value
      end
      sql = "INSERT INTO resources_values (resource_id, value_id) VALUES #{inserts.join(", ")}"
      ActiveRecord::Base.connection.execute sql
      page_token = items_response.data['nextPageToken']
    end while page_token
  end
end