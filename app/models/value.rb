class Value < ActiveRecord::Base
  attr_accessible :name, :resources_count
  belongs_to :facet
  has_and_belongs_to_many :resources
  before_destroy :delete_resources
  
  def update_resources_count
    self.resources_count = self.resources.count
    self.save
  end

  private
  
  def delete_resources
    resources.delete_all
  end
end
